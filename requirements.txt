-i https://pypi.org/simple
annotated-types==0.7.0; python_version >= '3.8'
babel==2.16.0; python_version >= '3.8'
bibtexparser==1.4.3
certifi==2024.12.14; python_version >= '3.6'
charset-normalizer==3.4.1; python_version >= '3.7'
click==8.1.8; python_version >= '3.7'
colorama==0.4.6; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6'
dnspython==2.7.0; python_version >= '3.9'
email-validator==2.2.0
freezegun==1.5.1
ghp-import==2.1.0
idna==3.10; python_version >= '3.6'
jinja2==3.1.5; python_version >= '3.7'
markdown==3.7; python_version >= '3.8'
markupsafe==3.0.2; python_version >= '3.9'
mergedeep==1.3.4; python_version >= '3.6'
mkdocs==1.6.1
mkdocs-autorefs==1.3.0; python_version >= '3.9'
mkdocs-get-deps==0.2.0; python_version >= '3.8'
mkdocs-material==9.5.49
mkdocs-material-extensions==1.3.1; python_version >= '3.8'
mkdocstrings==0.24.1
packaging==24.2; python_version >= '3.8'
paginate==0.5.7
pathspec==0.12.1; python_version >= '3.8'
platformdirs==4.3.6; python_version >= '3.8'
pydantic[email]==2.10.5
pydantic-core==2.27.2; python_version >= '3.8'
pygments==2.19.1; python_version >= '3.8'
pymdown-extensions==10.14; python_version >= '3.8'
pyparsing==3.2.1; python_version >= '3.9'
python-dateutil==2.9.0.post0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pyyaml==6.0.2; python_version >= '3.8'
pyyaml-env-tag==0.1; python_version >= '3.6'
regex==2024.11.6; python_version >= '3.8'
requests==2.32.3; python_version >= '3.8'
six==1.17.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
typing-extensions==4.12.2; python_version >= '3.8'
urllib3==2.3.0; python_version >= '3.9'
watchdog==6.0.0; python_version >= '3.9'
